Utilicé MySql como base de datos
En el archivo que esta Backend/server/datasources.json
Se encuentra la configuración de la base de datos. 
{
  "MySqlDb": {
    "host": "localhost",
    "port": 3306,
    "database": "EasyCreditdb",
    "password": "123456789",
    "name": "MySqlDb",
    "user": "root",
    "connector": "mysql"
  }
}

importante intalar paquetes: npm install

Para crear las tablas una vez que se configuro la conexión se ejecuta el siguiente comando que migra los modelos del back a la base de datos
node_modules/.bin/slc-migrate

Y despues de eso simplemente corres el proyecto con node..


En el front utilice react
Para correr el proyecto en react simplemente entras a la carpeta frontend  intalas los paquetes con npm install
y lo corres con npm run start