import axios from 'axios'
import { PATH_BASE } from '../../constants';

const login = ( usuario ) => {
    return axios.post(`${PATH_BASE}/usuarios/login`, usuario);
}

export {
    login
}