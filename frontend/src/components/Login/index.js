import React, { Component } from "react";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { Redirect } from 'react-router-dom'
import { login } from "./service";
import './index.css';

export default class Login extends Component {
    constructor() {
        super();

        this.state = {
            nombre: '',
            isLogin: false
        }

        this.onChangeName = this.onChangeName.bind(this);
        this.entrar = this.entrar.bind(this);
    }

    //Método que escucha al input del nombre
    onChangeName(e) {
        this.setState({ nombre: e.target.value });
    }

    //Este es el método que se usa para iniciar sesión
    async entrar() {
        const { nombre } = this.state;
        try{
            let usuario = await login({ nombre: nombre });  
            //Agregar el usuario al localStorage para guardar la sesión
            localStorage.setItem('userName', usuario.data.nombre);
            localStorage.setItem('userId', usuario.data.id);
            this.setState({isLogin: true})
        }catch(err){
            console.log(err);
        }
    }

    render() {
        const { nombre } = this.state;
        if(localStorage.getItem('userName')) {
            if(localStorage.getItem('userName') === 'ADMIN')
                return <Redirect to="/admin"> </Redirect>
            else
                return <Redirect to="/"> </Redirect>
        }
        return (
            <div className="login-container">
                <Card>
                    <CardContent>
                        <Typography variant="h5" component="h2">
                            Iniciar Sesión
                        </Typography>
                        <label>Para ingresar escriba su nombre por favor</label>
                        <form>
                            <TextField
                                label="Nombre"
                                value={nombre}
                                onChange={this.onChangeName}
                                margin="normal"
                                fullWidth
                                onKeyPress={(ev) => {
                                    if (ev.key === 'Enter') {
                                        this.entrar()
                                        ev.preventDefault();
                                    }}
                                }
                                />
                        </form>
                    </CardContent>
                    <CardActions className="card-action">
                        <Button variant="contained" color="primary" onClick={this.entrar}>Entrar</Button>
                    </CardActions>
                </Card>
            </div>
        )
    }
}
