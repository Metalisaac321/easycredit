import axios from 'axios'
import { PATH_BASE } from '../../constants';

//Método para obtener todas las solicitudes
const getSolicitudes = () => {
    return axios.get(`${PATH_BASE}/solicitudes?&filter[where][status]=0&filter[include]=plazo&filter[include]=usuario`);  
}

const procesarSolicitud = (solicitud) => {
    return axios.post(`${PATH_BASE}/solicitudes/procesarSolicitud`, solicitud);
}

export {
    getSolicitudes,
    procesarSolicitud
}