import React, { Component } from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import Tabla from "./Tabla";

const Solicitudes = ({ solicitudes, procesarSolicitud }) =>
  <Card className="card-margin">
    <Typography className="card-header" variant="h5" component="h2">
      Solicitudes actuales
        </Typography>
    <CardContent>
      {solicitudes.length > 0 ?
        <Tabla
          headers={['Nombre persona','Monto', 'Plazo', 'Interes', 'Total', 'Autorizar crédito']}
          rows={ solicitudes } 
          procesarSolicitud={procesarSolicitud}
          />
        : null}
    </CardContent>
  </Card>

export default Solicitudes;
