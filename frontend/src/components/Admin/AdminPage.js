import React, { Component } from "react";
import Solicitudes from './Solicitudes'
import { procesarSolicitud, getSolicitudes, getHistorial } from './service';

class AdminPage extends Component {
  constructor() {
    super();

    this.state = {
      solicitudes: []
    }

    this.procesarSolicitud = this.procesarSolicitud.bind(this);
  }

  async componentDidMount(){
    let solicitudes = await getSolicitudes();    
    this.setState({solicitudes: solicitudes.data})
  }

  async procesarSolicitud(solicitud) {
    let p = await procesarSolicitud(solicitud);
    let solicitudes = this.state.solicitudes.filter(s => s.id !== p.data.id);
    this.setState({ solicitudes });

  }
  
  render() {
    const { solicitudes } = this.state;
    return (
      <div className="container"> 
        <Solicitudes 
          solicitudes={solicitudes}
          procesarSolicitud={ this.procesarSolicitud } />
      </div>
      
    )
  }
}

export default AdminPage;
