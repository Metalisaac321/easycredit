import React, { Component } from "react";
import { NavBar } from "../Global";
import { Redirect } from 'react-router-dom'
import AdminPage from "./AdminPage";

export default class Admin extends Component {
  render() {
    return (
      <div>
        {localStorage.getItem('userName') === 'ADMIN'?
          <div>
            <NavBar />
            <AdminPage></AdminPage>
          </div>
          : <Redirect to="/login" />
        }
      </div>
    )
  }
}
