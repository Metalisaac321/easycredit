import React from "react";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';

const Tabla = ({ headers, rows, procesarSolicitud }) =>
    <Paper>
        <Table>
            <TableHead>
                <TableRow>
                    {headers.map(h => <TableCell>{ h }</TableCell>)}
                </TableRow>
            </TableHead>
            <TableBody>
                {rows.map(row => {
                    return (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row">
                                {row.usuario.nombre}
                            </TableCell>
                            <TableCell>
                                {row.monto}
                            </TableCell>
                            <TableCell>{row.plazo.descripcion}</TableCell>
                            <TableCell>{row.plazo.interes}%</TableCell>
                            <TableCell>{row.total}</TableCell>
                            <TableCell> 
                                <IconButton color="primary" onClick={() => procesarSolicitud({solicitud: row, aceptada: true})}>
                                    <Icon>thumb_up</Icon>
                                </IconButton>
                                <IconButton color="secondary" onClick={() => procesarSolicitud({solicitud: row, aceptada: false})}>
                                    <Icon>clear</Icon>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    );
                })}
            </TableBody>
        </Table>
    </Paper>

export default Tabla;
