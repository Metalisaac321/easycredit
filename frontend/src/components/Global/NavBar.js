import React, { Component } from "react";
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {Redirect} from 'react-router-dom';
import './index.css'

export default class NavBar extends Component {
    constructor(){
        super();
        this.state = {
            logout: false
        }
        this.onLogout = this.onLogout.bind(this);
    }

    //Método para desloguear al usuario
    onLogout() {
        localStorage.clear();
        this.setState({ logout: true })
    }

    render() {
        const { logout } = this.state;
        if(logout){
            return <Redirect to="/login"></Redirect>
        }
        return(
            <AppBar position="static">
                <Toolbar className="toolbar">
                    <Typography variant="h6" color="inherit">
                    EasyCredit
                    </Typography>
                    <Typography variant="subtitle1" color="inherit">
                        Bienvenido {localStorage.getItem('userName') }
                    </Typography>
                    <Button color="inherit" onClick={this.onLogout}>Cerrar Sesión</Button>
                </Toolbar>
            </AppBar>
        )
    }
}