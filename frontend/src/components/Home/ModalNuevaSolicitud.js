import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';

import { getPlazos } from './service';
import './index.css';

class ModalNuevaSolicitud extends Component {
    constructor(){
        super();
        this.state = {
            monto: 1,
            edad: '',
            tieneTarjeta: true,
            plazo: { descripcion: '', id: '', interes: 0, frecuencia:0 },
            total: 0,
            plazos: [],
            errores: ''
        }
        this.onEnviarSolicitud = this.onEnviarSolicitud.bind(this);
        this.onCheckboxChange = this.onCheckboxChange.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    
    async componentDidMount() {
        let plazos = await getPlazos();        
        this.setState({ plazos: plazos.data })
    }

    onEnviarSolicitud = () => {
        let solicitud = {...this.state};
        if(solicitud.plazo.interes < 1){
            return this.setState({errores: 'Seleccione un plazo antes de continuar'})
        }
        if(solicitud.total < 1){
            return this.setState({errores: 'Escriba un monto antes de continuar'})
        }
        if(!solicitud.edad)
            return this.setState({errores: 'Escriba su edad antes de continuar'})
        if(solicitud.edad < 20){
            return this.setState({errores: 'Lo sentimos no podemos procesar su solicitud, solo aceptamos personas de 20 años en adelante'})
        }
        this.setState({
            monto: 1,
            edad: '',
            tieneTarjeta: true,
            plazo: { descripcion: '', id: '', interes: 0, frecuencia:0 },
            total: 0,
            errores: ''
        });

        this.props.enviarSolicitud( solicitud )
    };

    onCheckboxChange = name => event => {
        this.setState({ [name]: event.target.checked });
    };

    onChange( e ) {
        
        if( e.target.name === 'plazo' ) {
            let monto = Number(this.state.monto);
            let plazo = e.target.value;
            let interes = Number(plazo.interes);
            let frecuencia = Number(plazo.frecuencia);
            let total = (monto * interes / 100 * frecuencia) + monto;            
            this.setState({ total })

        }else if(e.target.name === 'monto'){
            const { plazo } = this.state;
            let monto = Number(e.target.value);
            let interes = Number(plazo.interes);
            let frecuencia = Number(plazo.frecuencia);
            let total = monto;

            if(plazo.frecuencia > 0)
                total = (monto * interes / 100 * frecuencia) + monto;
            this.setState({ total })
        }
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        const { openModal, switchModal } = this.props;
        const { monto, edad, tieneTarjeta, plazo, plazos, total, errores } = this.state;
        return (
            <Dialog
                open={openModal}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Solicitud de crédito</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Por favor llene su solicitud de crédito
                    </DialogContentText>
                    <TextField style={{marginBottom: '20px'}}
                        name="monto"
                        margin="dense"
                        label="Monto"
                        type="number"
                        value={ monto }
                        onChange={ this.onChange }
                        fullWidth
                    />
                    <TextField style={{marginBottom: '20px'}}
                        name="edad"
                        margin="dense"
                        label="Edad"
                        type="number"
                        vale={ edad }
                        onChange={ this.onChange }
                        fullWidth
                    />
                    <FormControl >
                        <InputLabel htmlFor="plazo">Plazo</InputLabel>
                        <Select
                            value={ plazo }
                            onChange={ this.onChange }
                            fullWidth
                            inputProps={{
                                name: 'plazo',
                                id: 'plazo',
                            }}>
                            {plazos.map(p => 
                                <MenuItem key={p.id} value={p}> {p.descripcion} </MenuItem>
                            )}
                        </Select>

                        <FormControlLabel
                        control={
                            <Checkbox
                                checked={ tieneTarjeta }
                                onChange={ this.onCheckboxChange('tieneTarjeta') }
                                value="tieneTarjeta"
                                color="primary"
                            />
                        }
                        label="Tiene usted tarjeta de crédito?"
                    />
                    </FormControl>
                    <Typography variant="h5" component="h2">
                        Total a pagar después del plazo: { total } pesos
                    </Typography>
                </DialogContent>
                
                <DialogActions>
                    <Button onClick={this.onEnviarSolicitud} color="primary">
                        Enviar Solicitud
                    </Button>
                    <Button onClick={switchModal} color="primary">
                        Cancelar
                    </Button>
                </DialogActions>
                <Typography variant="subtitle1" color="error" gutterBottom>
                    { errores }
                </Typography>
            </Dialog>
        )
    }
}

export default ModalNuevaSolicitud;
