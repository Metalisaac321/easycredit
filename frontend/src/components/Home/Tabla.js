import React from "react";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const status = (status) =>{
    if(status === 0){ 
        return (<div>
            <Icon style={{color: 'orange'}}>
                radio_button_checked
            </Icon>
            <span>Pendiente</span>
        </div>)
    }
    if(status === 1){ 
        return (<div>
            <Icon style={{color: 'green'}}>
                radio_button_checked
            </Icon>
            <span>Aceptada</span>
        </div>)
    }
    if(status === 2){ 
        return (<div>
            <Icon style={{color: 'red'}}>
                radio_button_checked
            </Icon>
            <span>Rechazada</span>
        </div>)
    }
}
const Tabla = ({ headers, rows }) =>
    <Paper>
        <Table>
            <TableHead>
                <TableRow>
                    {headers.map(h => <TableCell>{ h }</TableCell>)}
                </TableRow>
            </TableHead>
            <TableBody>
                {rows.map(row => {
                    return (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row">
                                {row.monto}
                            </TableCell>
                            <TableCell>{row.plazo.descripcion}</TableCell>
                            <TableCell>{row.plazo.interes}%</TableCell>
                            <TableCell>{row.total}</TableCell>
                            <TableCell>{ status(row.status) }</TableCell>
                        </TableRow>
                    );
                })}
            </TableBody>
        </Table>
    </Paper>

export default Tabla;
