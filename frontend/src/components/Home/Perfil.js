import React, { Component } from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import './index.css';

const Perfil = ({ solicitud }) => 
    <Card className="card-margin">
        <Typography className="card-header" variant="h5" component="h2">
            Perfil
        </Typography>
      <CardContent>
        <Typography variant="subtitle1" gutterBottom>
          Nombre usuario: {localStorage.getItem('userName')}
        </Typography>
        <Typography variant="subtitle1" gutterBottom>
          Ultima peticion
        </Typography>
      </CardContent>
    </Card>

export default Perfil;
