import React, { Component } from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import Tabla from "./Tabla";

const Solicitudes = ({ solicitudes, onNuevaSolicitud }) =>
  <Card >
    <Typography className="card-header" variant="h5" component="h2">
      Solicitudes actuales
        </Typography>
    <CardContent>
      {solicitudes.length > 0 ?
        <Tabla
          headers={['Monto', 'Plazo', 'Interes', 'Total', 'Status']}
          rows={ solicitudes } />
        : null}
        <Button 
          variant="outlined" 
          color="primary" 
          style= {{marginTop: '25px'}} 
          onClick={onNuevaSolicitud}>
          Nueva solicitud
        </Button>
    </CardContent>
  </Card>

export default Solicitudes;
