import React, { Component } from "react";
import Perfil from './Perfil'
import Solicitudes from './Solicitudes'
import Historial from "./Historial";
import ModalNuevaSolicitud from "./ModalNuevaSolicitud";
import { enviarSolicitud, getSolicitudes, getHistorial } from './service';

import './index.css';

class HomePage extends Component {
  constructor() {
    super();

    this.state = {
      openModal: false,
      solicitudes: [],
      historial: []
    }

    this.enviarSolicitud = this.enviarSolicitud.bind(this);
    this.switchModal = this.switchModal.bind(this);
  }

  async componentDidMount(){
    let solicitudes = await getSolicitudes();
    let historial = await getHistorial();
    console.log('historial ', historial);
    
    this.setState({solicitudes: solicitudes.data})
    this.setState({historial: historial.data})
  }

  //Metodo para abrir o cerrar el modal
  switchModal() {
    this.setState({ openModal: !this.state.openModal })
  }

  async enviarSolicitud(solicitud) {
    solicitud.usuarioId = localStorage.getItem('userId');
    let nuevaSolicitud = await enviarSolicitud(solicitud);    
    this.setState({ solicitudes: [...this.state.solicitudes, nuevaSolicitud.data]});
    this.switchModal();
  }
  
  render() {
    const { openModal, solicitudes, historial } = this.state;
    return (
      <div>
        <div className="profile-container">
          <Perfil/>
        </div>
        <div className="container"> 
        
          {historial.length > 0? <Historial 
            historial={ historial } />
          :null}
          
          <Solicitudes 
            solicitudes={solicitudes}
            onNuevaSolicitud={ this.switchModal } />

          <ModalNuevaSolicitud 
            openModal={openModal}
            enviarSolicitud={ this.enviarSolicitud }
            switchModal={ this.switchModal } />
        </div>
      </div>
      
    )
  }
}

export default HomePage;
