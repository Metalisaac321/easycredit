import React, { Component } from "react";
import { NavBar } from "../Global";
import { Redirect } from 'react-router-dom'
import HomePage from "./HomePage";

class Home extends Component {
  render() {
    return (
      <div>
        {localStorage.getItem('userName')?
          <div>
            <NavBar />
            <HomePage></HomePage>
          </div>
          : <Redirect to="/login" />
        }
      </div>
    )
  }
}

export default Home;
