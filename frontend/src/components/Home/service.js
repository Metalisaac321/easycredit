import axios from 'axios'
import { PATH_BASE } from '../../constants';

const getPlazos = () => {
    return axios.get(`${PATH_BASE}/plazos`);
}

const enviarSolicitud = async (solicitud) => {
    let s = await axios.post(`${PATH_BASE}/solicitudes`, solicitud);
    return axios.get(`${PATH_BASE}/solicitudes/${s.data.id}?filter[include]=plazo`); 
}

//Obtiene las solicitudes del usuario logueado
const getSolicitudes = () => {
    return axios.get(`${PATH_BASE}/solicitudes?filter[where][and][0][usuarioId]=${localStorage.getItem('userId')}&filter[where][and][1][status]=0&filter[include]=plazo`);  
}

const getHistorial = () =>{
    return axios.get(`${PATH_BASE}/solicitudes?filter[where][usuarioId]=${localStorage.getItem('userId')}&filter[where][or][0][status]=1&filter[where][or][1][status]=2&filter[include]=plazo`);  
}

export {
    getPlazos,
    enviarSolicitud,
    getSolicitudes,
    getHistorial
}