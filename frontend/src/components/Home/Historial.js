import React, { Component } from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Tabla from "./Tabla";
import './index.css';

const Historial = ({ historial }) =>
    <Card className="card-margin">
        <Typography className="card-header" variant="h5" component="h2">
            Historial crediticio
        </Typography>
        <CardContent>
            <Tabla 
            headers={['Monto', 'Plazo', 'Interes', 'Total', 'Status']} 
            rows={historial}/> 
        </CardContent>
    </Card>

export default Historial;
