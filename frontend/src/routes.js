import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { Login, Home, Admin} from './components';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact component={ Home } />
      <Route path="/login" exact component={ Login }/>
      <Route path="/admin" exact component={ Admin }/>
    </Switch>
  </BrowserRouter>
);

export default Routes;
