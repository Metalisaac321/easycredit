'use strict';

module.exports = function( Usuario ) {
    /**
    * Esta ruta es para que el usuario ingrese en la plataforma con su nombre de usuario, crear la cuenta sino
    * existe y si ya existe mandar un error
    * @param {object} usuario Este es el objeto usuario que se va a loguear
    * @param {Function(Error, object)} callback
    */

    Usuario.login = async ( usuarioNuevo ) => {
        //Convertir a Mayuscula para que no se repitan nombres
        usuarioNuevo.nombre = usuarioNuevo.nombre.toUpperCase();
        try {
            let usuario = await Usuario.findOne({where: {nombre: usuarioNuevo.nombre}});
            if( !usuario )
                usuario = await Usuario.create(usuarioNuevo)
            
            return usuario;

        }catch(err){
            return err;
        }
    };

};
