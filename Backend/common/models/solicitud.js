'use strict';

module.exports = function(Solicitud) {
    /**
     * Este metodo es para procesar una solicitud como aceptada o rechazada
     * @param {object} aceptada Esta es la solicitud
     * @param {Function(Error, object)} callback
     */

    Solicitud.procesarSolicitud = async ( solicitud ) => {
        try{
            let s = await Solicitud.findById(solicitud.solicitud.id);
            if(solicitud.aceptada)
                s.status = 1;
            else
                s.status = 2;

            await s.save();
            
            return s;
        }catch(err){
            return err;
        }
        
    };

};
