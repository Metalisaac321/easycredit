//Esto se ejecuta cuando inicia el servidor( es la carga inicial lo que se necesita para iniciar la aplicación)
module.exports = async (app) => {
    var Usuario = app.models.Usuario;
    var Plazo = app.models.Plazo;

    try {
        //Crear el usuario por default admin
        let admin = await Usuario.find({ where: { nombre: 'ADMIN' } });

        if (  admin.length === 0 ) {
            let u = await Usuario.create({ nombre: 'ADMIN' });
            console.log('Admin', u);   
        }

        let plazos = await Plazo.find();

        if ( plazos.length ===  0 ) {
            let plazos = await Plazo.create([
                { descripcion: '3 pagos', interes: 5, frecuencia: 3 },
                { descripcion: '6 pagos', interes: 7, frecuencia: 6 },
                { descripcion: '9 pagos', interes: 12, frecuencia: 9 },
            ]);
            console.log('Plazos', plazos);
            
        }

    } catch (err) {
        console.log(err);
    }

};